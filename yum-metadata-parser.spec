%global _empty_manifest_terminate_build 0
%global __python python3
Name:		yum-metadata-parser
Version:	1.1.4
Release:        25
Summary:	A fast metadata parser for yum
License:	GPLv2
URL:		http://yum.baseurl.org/
Source0: 	http://yum.baseurl.org/download/%{name}/%{name}-%{version}.tar.gz
Source1: 	https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Patch0: 	BZ-612409-handle-2GB-rpms.patch
Patch1: 	UPSTREAM-py-3-split.patch
Patch2: 	UPSTREAM-weak-deps.patch
Patch3: 	UPSTREAM-index-weak-deps.patch
Patch4: 	UPSTREAM-fix-minor-mem-leak.patch

BuildRequires: python3-devel glib2-devel libxml2-devel sqlite-devel pkgconfig
BuildRequires:	python3-setuptools
BuildRequires: python3-pbr
Requires:	glib2 >= 2.15

%description
Fast metadata parser for yum implemented in C.

%package	help
Summary: 	Doc files for %{name}

%description 	help
The %{name}-help package contains doc files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1 -S git
cp %{SOURCE1} .

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --root=%{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS
%license gpl-2.0.txt
%{python3_sitearch}/*

%files help
%doc README ChangeLog

%changelog
* Wed May 19 2021 baizhonggui <baizhonggui@huawei.com> - 1.1.4-25
- Fix replaceing python2-devel to python3-devel error

* Wed Aug 28 2019 luhuaxin <luhuaxin@huawei.com> - 1.1.4-24
- Package init
